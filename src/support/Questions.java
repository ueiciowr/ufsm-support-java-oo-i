/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package support;

import java.text.Normalizer;
import java.util.HashSet;
import java.util.StringTokenizer;

/**
 *
 * @author rwietter padronizar o texto de input, retornar um array de palavras
 */
public class Questions {

    HashSet words;

    public HashSet normalizeWord(String phrase) {
        phrase = phrase.replace("?", "");

        phrase = Normalizer.normalize(phrase, Normalizer.Form.NFD).replace("[^\\p{ASCII}]", "");
        StringTokenizer splitWords = new StringTokenizer(phrase);

        words = new HashSet();

        while (splitWords.hasMoreElements()) {
            words.add(splitWords.nextToken());
        }

        return words;
    }

}
