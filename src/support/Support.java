/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package support;

import java.util.HashSet;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author rwietter
 */
public class Support {

    private Questions questions;
    private Answers answers;

    public Support() {
        questions = new Questions();
        answers = new Answers();
    }

    public String processQuestionsAnswers(String phrase) {

        HashSet wordGroup = questions.normalizeWord(phrase);

        String response = answers.AnswerGanerator(wordGroup);

        return response;
    }

    public void welcome() {
        String message = ""
                + "Welcome to the support system!"
                + "\nPlease, speak about your problem"
                + "\nWe will ajudá-lo";

        JFrame frame = new JFrame("jOptionPane showMessage Dialog");

        JOptionPane.showMessageDialog(frame, message + "'", "Support", JOptionPane.INFORMATION_MESSAGE);
    }
}
