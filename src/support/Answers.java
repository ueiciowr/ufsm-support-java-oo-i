/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package support;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;

/**
 *
 * @author rwietter
 */
public class Answers {

    private final HashMap answers;
    private final ArrayList genericAnswers;
    private final Random randomAnswers;

    public Answers() {
        answers = new HashMap();
        genericAnswers = new ArrayList();
        mapCompleteAnswers();
        standardAnswers();
        randomAnswers = new Random();
    }

    private void mapCompleteAnswers() {
        answers.put("oi", "Oi, em que posso ajudar ?");
        answers.put("boa noite", "Boa noite");
        answers.put("olá", "Olá, em que posso ajudar");
        answers.put("boa tarde", "Boa tarde");
        answers.put("estou com um problema", "Qual é o seu problema, como posso ajudá-lo(a) ?");
    }

    private void standardAnswers() {
        genericAnswers.add("Não foi possível atender este chamado");
        genericAnswers.add("Vou passar para o atendente");
        genericAnswers.add("Em que posso ajudá-lo(a) ?");
    }

    private String chooseRandomAnswers() {
        int idx = randomAnswers.nextInt(genericAnswers.size());

        String randomAnswer = (String) genericAnswers.get(idx);

        return randomAnswer;
    }

    public String AnswerGanerator(HashSet words) {
        Iterator iterator = words.iterator();

        while (iterator.hasNext()) {
            String word = (String) iterator.next();
            String answer = (String) answers.get(word);

            if (answer != null) {
                return answer;
            }
        }
        return chooseRandomAnswers();
    }
}
